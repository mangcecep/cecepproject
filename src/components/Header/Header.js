import React from 'react'
import './Header.css'

import {BrowserRouter as Router,  Link}  from 'react-router-dom'
import {FaAlignRight} from 'react-icons/fa'

function Header() {
    const [toggle, setToggle] = React.useState(false)

    const Toggle = () => {
        setToggle({toggle: !toggle})
    }
    return (
        <>
            <div className="navBar">
                <button onClick={Toggle}>
                    <FaAlignRight/>
                </button>
                <ul className={toggle ? "nav-links show-nav" : "nav-links"} >
                    <Router>
                        <Link to="/">Home</Link>
                        <Link to="/">About us</Link>
                        <Link to="/">Contact</Link>
                    </Router>
                </ul>
            </div>
        </>

    )
}

export default Header
